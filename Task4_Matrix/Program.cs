﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixLibrary;

namespace Task4_Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            TestMatrixClass();
        }

        public static void TestMatrixClass()
        {
            try
            {
                double[,] arr1 = new double[3, 3] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
                double[,] arr2 = new double[3, 3] { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
                Matrix matr1 = new Matrix(arr1);
                Matrix matr2 = new Matrix(arr2);
                Console.WriteLine("Matrix1:\n{0}\n", matr1);
                Console.WriteLine("Matrix2:\n{0}\n", matr2);
                Console.WriteLine("Matrix1 + matrix2:\n{0}", matr1 + matr2);
                Console.WriteLine("Matrix1 - matrix2:\n{0}", matr1 - matr2);
                Console.WriteLine("Matrix1 * matrix2:\n{0}", matr1 * matr2);
                Console.WriteLine("Matrix2 * 5:\n{0}", matr2 * 5);
                Console.WriteLine("Is matrix1 equal to matrix2:{0}\n", matr1 == matr2);
                Console.WriteLine("Is matrix1 greater than matrix2:{0}\n", matr1 > matr2);
                Console.WriteLine("Get submatrix (2*2) from matrix1:\n{0}\n", matr1.SubMatrix(2, 2));
                matr1.Transpose();
                Console.WriteLine("Transposed matrix1:\n{0}\n", matr1);
                Console.WriteLine("Application runs successfully");
                Console.WriteLine("Press enter to exit");
            }
            catch (IndexOutOfRangeException indEx)
            {
                Console.WriteLine("Application crashed!");
                Console.WriteLine(indEx.Message);
            }
            catch (ArgumentNullException argNullEx)
            {
                Console.WriteLine("Application crashed!");
                Console.WriteLine(argNullEx.Message);
            }
            catch (ArgumentException argEx)
            {
                Console.WriteLine("Application crashed!");
                Console.WriteLine(argEx.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}

