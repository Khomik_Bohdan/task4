﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLibrary
{
    public class Matrix : IComparable<Matrix>
    {
        private double[,] _matrix;

        public int RowCount { get; private set; }
        public int ColumnCount { get; private set; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="row">Number of rows in matrix</param>
        /// <param name="column">Number of columns in matrix</param>
        public Matrix(int row, int column)
        {
            _matrix = new double[row, column];
            RowCount = row;
            ColumnCount = column;
        }
        /// <summary>
        /// Constructor that build matrix on given array
        /// </summary>
        /// <param name="matrix">Double dimensional array</param>
        public Matrix(double[,] matrix)
        {
            _matrix = matrix;
            RowCount = matrix.GetLength(0);
            ColumnCount = matrix.GetLength(1);
        }
        /// <summary>
        /// Indexer of the matrix
        /// </summary>
        /// <param name="row">Integer value,represents needed row</param>
        /// <param name="column">Integer value,represents needed column</param>
        /// <returns>Double value at the needed position</returns>
        public double this[int row, int column]
        {
            get
            {
                if (row < 0 || row >= RowCount || column < 0 || column >= ColumnCount)
                    throw new IndexOutOfRangeException("Incorect Index!");
                return _matrix[row, column];
            }
            set
            {
                if (row < 0 || row >= RowCount || column < 0 || column >= ColumnCount)
                    throw new IndexOutOfRangeException("Incorect Index!");
                _matrix[row, column] = value;
            }
        }
        /// <summary>
        /// Overloaded operator for subtraction two matrixes
        /// </summary>
        /// <param name="m1">First matrix to be added</param>
        /// <param name="m2">Second matrix to be added</param>
        /// <returns>New matrix</returns>
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if (m1 is null || m2 is null)
                throw new ArgumentException("Can not add null matrix!");
            if ((m1.RowCount != m2.RowCount) || (m1.ColumnCount != m2.ColumnCount))
                throw new ArgumentException("Sizes of matrixes are not equal!");
            Matrix resultMatrix = new Matrix(m1.RowCount, m1.ColumnCount);
            for (int i = 0; i < resultMatrix.RowCount; i++)
            {
                for (int j = 0; j < resultMatrix.ColumnCount; j++)
                {
                    resultMatrix[i, j] = m1[i, j] + m2[i, j];
                }
            }
            return resultMatrix;
        }
        /// <summary>
        /// Overloaded operator for subtraction two matrixes
        /// </summary>
        /// <param name="m1">First matrix to be subtracted</param>
        /// <param name="m2">Second matrix to be subtracted</param>
        /// <returns>New matrix</returns>
        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if (m1 is null || m2 is null)
                throw new ArgumentException("Can not subtrack null matrix!");
            if ((m1.RowCount != m2.RowCount) || (m1.ColumnCount != m2.ColumnCount))
                throw new ArgumentException("Sizes of matrixes are not equal!");
            Matrix resultMatrix = new Matrix(m1.RowCount, m1.ColumnCount);
            for (int i = 0; i < resultMatrix.RowCount; i++)
            {
                for (int j = 0; j < resultMatrix.ColumnCount; j++)
                {
                    resultMatrix[i, j] = m1[i, j] - m2[i, j];
                }
            }
            return resultMatrix;
        }
        /// <summary>
        /// Overloaded operator for multiplying two matrixes
        /// </summary>
        /// <param name="m1">First matrix to multiply</param>
        /// <param name="m2">Second matrix to multiply</param>
        /// <returns>New matrix</returns>
        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1 is null || m2 is null)
                throw new ArgumentNullException("Can not multiply null matrixes!");

            if (m1.ColumnCount != m2.RowCount)
                throw new ArgumentException("Matrixes have not proper dimension!");

            Matrix resultMatrix = new Matrix(m1.RowCount, m2.ColumnCount);
            for (int i = 0; i < resultMatrix.RowCount; i++)
            {
                for (int j = 0; j < resultMatrix.ColumnCount; j++)
                {
                    resultMatrix[i, j] = 0;
                    for (int k = 0; k < resultMatrix.ColumnCount; k++)
                    {
                        resultMatrix[i, j] += (m1[i, k] * m2[k, j]);
                    }
                }
            }
            return resultMatrix;
        }
        /// <summary>
        /// Overloaded operator for multiplication matrix on a scalar value
        /// </summary>
        /// <param name="m">Matrix object</param>
        /// <param name="number">Double value represents a scalar number</param>
        /// <returns>New matrix</returns>
        public static Matrix operator *(Matrix m, double number)
        {
            if (m is null)
                throw new ArgumentNullException("Can not multiply null matrix!");

            Matrix resultMatrix = new Matrix(m.RowCount, m.ColumnCount);
            for (int i = 0; i < m.RowCount; i++)
            {
                for (int j = 0; j < m.ColumnCount; j++)
                {
                    resultMatrix[i, j] = m[i, j] * number;
                }
            }
            return resultMatrix;
        }
        /// <summary>
        /// Overloaded operator for multiplication matrix on a scalar value
        /// </summary>
        /// <param name="m">Matrix object</param>
        /// <param name="number">Double value represents a scalar number</param>
        /// <returns>New matrix</returns>
        public static Matrix operator *(double number, Matrix m)
        {
            return m * number;
        }
        /// <summary>
        /// Overloaded function for comparison two objects by their values
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Bool value represents does given object is equal to this object</returns>
        public override bool Equals(object obj)
        {
            Matrix matrix = obj as Matrix;
            if (matrix is null || matrix.RowCount != this.RowCount || matrix.ColumnCount != this.ColumnCount)
                return false;
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    if (matrix[i, j] != this[i, j])
                        return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Overloaded operator for comparison two matrixes
        /// </summary>
        /// <param name="m1">First matrix to be compared</param>
        /// <param name="m2">Second matrix to be compared</param>
        /// <returns>Bool value, indicates does two matrixes are equal</returns>
        public static bool operator ==(Matrix m1, Matrix m2)
        {
            if (m1 is null)
                throw new ArgumentNullException("Can not apply operation of equality to null object");
            return m1.Equals(m2);
        }
        /// <summary>
        /// Overloaded operator for comparison two matrixes
        /// </summary>
        /// <param name="m1">First matrix to be compared</param>
        /// <param name="m2">Second matrix to be compared</param>
        /// <returns>Bool value, indicates does two matrixes are not equal</returns>
        public static bool operator !=(Matrix m1, Matrix m2)
        {
            if (m1 is null)
                throw new ArgumentNullException("Can not apply operation of equality to null object");
            return !m1.Equals(m2);
        }
        /// <summary>
        ///Compare this matrix to given
        /// </summary>
        /// <param name="m">Matrix for comparison</param>
        /// <returns>integer value, indicates does this object is greater or smaller than given</returns>
        public int CompareTo(Matrix m)
        {
            if (m is null)
                throw new ArgumentNullException("A comparing matrix is equal to null");
            if (m.RowCount != this.RowCount || m.ColumnCount != this.ColumnCount)
                throw new ArgumentException("This matrixes have different size!");

            bool isGreater = true;
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    if (this[i, j] < m[i, j])
                        isGreater = false;
                }
            }
            if (isGreater)
                return 1;

            bool isSmaller = true;
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    if (this[i, j] > m[i, j])
                        isSmaller = false;
                }
            }
            if (isSmaller)
                return -1;

            return 0;
        }
        /// <summary>
        /// Overloaded operator that indicates which matrix is greater
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>Bool value</returns>
        public static bool operator >(Matrix m1, Matrix m2)
        {
            if (m1 is null)
                throw new ArgumentNullException("Can not apply operation of equality to null object");
            return m1.CompareTo(m2) > 0;
        }
        /// <summary>
        /// Overloaded operator that indicates which matrix is smaller
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>Bool value</returns>
        public static bool operator <(Matrix m1, Matrix m2)
        {
            if (m1 is null)
                throw new ArgumentNullException("Can not apply operation of equality to null object");
            return m1.CompareTo(m2) < 0;
        }
        /// <summary>
        /// Overloaded operator that indicates does the matrix is greater than or equal to given parameter
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>Bool value</returns>
        public static bool operator >=(Matrix m1, Matrix m2)
        {
            if (m1 is null)
                throw new ArgumentNullException("Can not apply operation of equality to null object");
            return m1.CompareTo(m2) >= 0;
        }
        /// <summary>
        /// Overloaded operator that indicates does the matrix is smaller than or equal to given parameter
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>Bool value</returns>
        public static bool operator <=(Matrix m1, Matrix m2)
        {
            if (m1 is null)
                throw new ArgumentNullException("Can not apply operation of equality to null object");
            return m1.CompareTo(m2) <= 0;
        }
        /// <summary>
        /// Transpose matrix
        /// </summary>
        public void Transpose()
        {
            if (RowCount != ColumnCount)
                throw new ArgumentException("This matrix can not be transposed!");

            double[,] matrix = new double[RowCount, ColumnCount];
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    matrix[i, j] = _matrix[i, j];
                }
            }
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    this[i, j] = matrix[j, i];
                }
            }
        }
        /// <summary>
        /// Get submatrix from the original matrix
        /// </summary>
        /// <param name="row">Count of needed rows</param>
        /// <param name="column">Count of needed columns</param>
        /// <returns>New matrix</returns>
        public Matrix SubMatrix(int row, int column)
        {
            if (row > RowCount || row < 0 || column > ColumnCount || column < 0)
                throw new ArgumentException("Incorrect bounds!");
            Matrix result = new Matrix(row, column);
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    result[i, j] = this[i, j];
                }
            }
            return result;
        }
        /// <summary>
        /// Represents matrix object as a string
        /// </summary>
        /// <returns>Formatted string</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    sb.Append(string.Format("{0} ", this[i, j]));
                }
                sb.Append(string.Format("\n"));
            }
            return sb.ToString();
        }
    }
}
